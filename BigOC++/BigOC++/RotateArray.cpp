#include <vector>
#include <string>
using namespace std;

class Solution {
public:
    void rotate(vector<int>& nums, int k) {
        int temp[1000000];
        for (int i = 0; i < nums.size(); i++)
        {
            temp[(i + k) % nums.size()] = nums.at(i);
        }
        for (int i = 0; i < nums.size(); i++)
        {
            nums.at(i) = temp[i];
        }
    }
};