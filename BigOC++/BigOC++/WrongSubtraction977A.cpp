#include <iostream>
#include <string>
using namespace std;

int mainWrongSubtraction()
{
	int n, k;
	cin >> n;
	cin >> k;

	int result = n;
	for (int i = 0; i < k; i++)
	{
		string temp = to_string(result);
		char ch = temp.at(temp.length() - 1);
		int endValue = ch - '0';
		if (endValue != 0)
		{
			result = result - 1;
		}
		else
		{
			result = result / 10;
		}
	}
	cout << result;

	return 0;
}