#include <iostream>
using namespace std;

bool isValidExpand(int xMove, int yMove, int(&board)[10][10], int m, int n, int(&block)[10][10])
{
	if (xMove >= 0 && xMove < n && yMove >= 0 && yMove < m)
	{
		if (board[xMove][yMove] == -1 && block[xMove][yMove] == 0)
		{
			return true;
		}
	}
	return false;
}

int knightTourExpandProblem(int step,int xMove,int yMove,int(&board)[10][10],int(&block)[10][10],int m,	int n,	int xCanMove[],	int yCanMove[],int numberBlock, int(&result)[10])
{
	if (step == (m * n - numberBlock))
	{
		return 1;
	}
	for (int i = 0; i < 8; i++)
	{
		int newXMove = xMove + xCanMove[i];
		int newYMove = yMove + yCanMove[i];
		if (isValidExpand(newXMove, newYMove, board, m, n, block))
		{
			board[newXMove][newYMove] = step;

			if (knightTourExpandProblem(step + 1, newXMove, newYMove, board, block, m, n, xCanMove, yCanMove,numberBlock, result) == 1)
			{

				return 1;
			}
			else
			{
				board[newXMove][newYMove] = -1;
			}

		}
	}

	return 0;

}

int mainProblem()
{
	int m, n;
	cin >> m;
	cin >> n;
	int numberBlock;
	cin >> numberBlock;

	return 0;
}