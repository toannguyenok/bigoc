#include <iostream>
using namespace std;

 bool isValid(int xMove, int yMove, int (&board)[10][10], int n)
{
     if (xMove >= 0 && yMove >= 0 && xMove < n && yMove < n)
     {
         if (board[xMove][yMove] == -1)
         {
             return true;
         }
     }
     return false;
}

 int knightTourProblem(int step,int xMove, int yMove, int(&board)[10][10], int n, int xCanMove[], int yCanMove[])
 {
     if (step == n * n)
     {
         return 1;
     }
     for (int i = 0; i < 8; i++)
     {
         int newXMove = xMove + xCanMove[i];
         int newYMove = yMove + yCanMove[i];
         if (isValid(newXMove, newYMove, board, n))
         {
             board[newXMove][newYMove] = step;
             if (knightTourProblem(step + 1, newXMove, newYMove, board, n, xCanMove, yCanMove) == 1)
             {
                 return 1;
             }
             else
             {
                 board[newXMove][newYMove] = -1;
             }
         
         }
     }
     return 0;
 }

 void printSolution(int n, int(&board)[10][10])
 {
     for (int i = 0; i < n; i++)
     {
         for (int j = 0; j < n; j++)
         {
             cout << board[i][j];
             cout << " ";
         }
         cout << "\n";
     }
 }

 int mainKnightTour()
 {
     int n;
     cin >> n;

     int board[10][10];
     for (int i = 0; i < 10; i++)
     {
         std::fill(board[i], board[i] + 10, -1);
     }
     int xCanMove[8] = { 2,1,-1,-2,-2,-1,1,2 };
     int yCanMove[8] = { 1,2,2,1,-1,-2,-2,-1 };
     board[0][0] = 0;
     int step = 1;
     knightTourProblem(1, 0, 0, board, n, xCanMove, yCanMove);
     printSolution(n, board);
     return 0;
 }
