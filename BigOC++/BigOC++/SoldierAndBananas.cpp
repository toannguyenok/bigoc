#include <iostream>
using namespace std;
#include <cmath>


int mainSoldierAndBananas() {
    int k, n, w;
    cin >> k;
    cin >> n;
    cin >> w;

    int totalPaid = 0;

    for(int i = 0; i < w; i++)
    {
        totalPaid = totalPaid + (i  + 1) * k;
    }
    int result = max(0, totalPaid - n);
    cout << result;
    return 0;
}