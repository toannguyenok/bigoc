#include <vector>
#include <string>
using namespace std;

class Solution {
public:
    vector<int> plusOne(vector<int>& digits) {
        return increase(digits.size() - 1, digits);
    }

    vector<int> increase(int position, vector<int>& digits)
    {
        int temp = digits[position];
        if (temp == 9)
        {
            temp = 0;
            digits[position] = temp;
            int nextPosition = position - 1;
            if (nextPosition < 0)
            {
                digits.insert(digits.begin(), 1);
                return digits;
            }
            return increase(nextPosition, digits);
        }
        else
        {
            digits[position] = ++temp;
        }
        return digits;
    }
};