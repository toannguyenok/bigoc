#include <vector>
#include <string>
using namespace std;

class Solution {
public:
    void moveZeroes(vector<int>& nums) {
     
        if (nums.size() == 1)
        {
            return;
        }
    
      for(int i = 0;i< nums.size();i++)
      { 
          if (nums[i] == 0)
          {
              for (int j = i + 1; j < nums.size(); j++)
              {
                  if (nums[j] != 0)
                  {
                      //swap
                      int temp = nums[i];
                      nums[i] = nums[j];
                      nums[j] = temp;
                      break;
                  }
              }
          }
      }
    }
};