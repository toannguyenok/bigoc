#include <iostream>

using namespace std;

int mainWordCapitalization()
{
	string input;
	cin >> input;
	string result = "";
	for (int i = 0; i < input.length(); i++)
	{
		char ch = input.at(i);
		if (i == 0 && ch >= 97)
		{
			ch = ch - 32;
		}
		result.append(1,ch);
	}
	cout << result;
	return 0;
}