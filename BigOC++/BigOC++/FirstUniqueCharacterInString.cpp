
#include <string>
#include <map>

using namespace std;

class Solution {
public:
    int firstUniqChar(string s) {
        map<char, int> m;
     
        int result = -1;
        for (int i = 0; i < s.length(); i++)
        {
         
            if (m.find(s[i]) == m.end()) {
                m[s[i]] = 1;
            }
            else {
                m[s[i]] = m[s[i]] + 1;
            }
        }

        for (int i = 0; i < s.length(); i++)
        {
           
            if ( m[s[i]] == 1)
            {
                result = i;
                return result;
            }
        }
    
        return -1;
    }
};