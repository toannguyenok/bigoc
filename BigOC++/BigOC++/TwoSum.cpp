#include <vector>
#include <string>
#include <unordered_map>
using namespace std;

class Solution {
public:
    int twoSum(vector<int>& nums, int left, int right) {
        //using hashmap
        unordered_map<int, vector<int>> umap;
     
        for (int i = 0; i < nums.size(); i++)
        {
     
              if (umap.find(nums[i]) != umap.end())
              {
                   vector<int> temp = umap[nums[i]];
                   temp.push_back(i);
                  umap[nums[i]] = temp;
             }
             else
                {
                vector<int> temp;
                temp.push_back(i);
                umap[nums[i]] = temp;
              }
            }
        int abc = 0;
        for (int target = left; target <= right; target++)
        {
         
            vector<int> result;
            for (int i = 0; i < nums.size(); i++)
            {

                if (umap.find(target - nums[i]) != umap.end())
                {
                    vector<int> temp = umap[target - nums[i]];
                    if (temp.size() > 1)
                    {
                        for (int j = 0; j < temp.size(); j++)
                        {
                            result.push_back(temp[j]);
                        }
                        break;
                    }
                    else
                    {
                        if (temp[0] != i)
                        {
                            result.push_back(temp[0]);
                        }
                    }
                }

            }
            abc += result.size();
        }
        return abc;
    }
};