
#include <vector>
#include <string>
using namespace std;


class Solution {
public:
	vector<vector<string>> solveNQueens(int n) {
		int board[10][10];
		for (int i = 0; i < 10; i++)
		{
			std::fill(board[i], board[i] + 10, 0);
		}

		vector<vector<string>> result;
		for (int i = 0; i < n; i++)
		{
			nQueen(i, n, board, result);
		}


		return result;
	}

	void nQueen(int row, int n, int(&board)[10][10], vector<vector<string>>& result)
	{
		if (row == n)
		{
			vector<string> child;
			for (int i = 0; i < n; i++)
			{
				string s = "";
				int countEmpty = 0;
				for (int j = 0; j < n; j++)
				{
					if (board[i][j] == 1) {
						s.append("Q");
					}
					else
					{
						countEmpty++;
						s.append(".");
					}
				}
				if (countEmpty == n)
				{
					return;
				}
				child.push_back(s);
			}
			result.push_back(child);
			return;
		}
		for (int j = 0; j < n; j++)
		{
			if (checkValid(row, board, n, j))
			{
				board[row][j] = 1;
				nQueen(row + 1, n, board, result);
				board[row][j] = 0;
			}
		}
	}


	bool checkValid(int row, int(&board)[10][10], int n, int column)
	{
		//check vertical
		for (int i = 0; i < row; i++)
		{
			if (board[i][column] == 1)
			{
				return false;
			}
			//check main
			for (int i = row, j = column; i >=0 && j >=0; i--, j--)
			{
				if (board[i][j] == 1)
				{
					return false;
				}
			}
			//check second
			for (int i = row, j = column; i >= 0 && j < n; i--, j++)
			{
				if (board[i][j] == 1)
				{
					return false;
				}
			}
		}
		return true;
	}

};