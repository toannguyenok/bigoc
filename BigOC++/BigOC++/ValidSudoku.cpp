#include <vector>
#include <string>
using namespace std;


class Solution {
public:
	bool isValidSudoku(vector<vector<char>>& board) {
		for (int i = 0; i < 9; i++)
		{
			for (int j = 0; j < 9; j++) {
				if (board[i][j] != '.')
				{
					bool validRow = checkRow(i, j, board);
					bool validColumn = checkColumn(i, j, board);
					bool validSquare = checkSquare(i, j, board, 3 * (i / 3), 3 * (j / 3));
					if (!validRow || !validColumn || !validSquare) {
						return false;
					}
				}
			}
		}
		return true;
	}

	bool checkRow(int row, int column, vector<vector<char>>& board)
	{
		for (int i = 0; i < 9; i++)
		{
			if (i != column)
			{
				if (board[row][i] == board[row][column])
				{
					return false;
				}
			}
		}
		return true;
	}

	bool checkColumn(int row, int column, vector<vector<char>>& board)
	{
		for (int i = 0; i < 9; i++)
		{
			if (i != row)
			{
				if ( board[i][column] == board[row][column])
				{
					return false;
				}
			}
		}
		return true;
	}

	bool checkSquare(int row, int column, vector<vector<char>>& board, int startRow, int startColumn)
	{

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++)
			{
				if (startRow + i != row && startColumn + j != column)
				{
					if ( board[startRow + i][startColumn + j] == board[row][column])
					{
						return false;
					}
				}
			}
		}
		return true;
	}

};