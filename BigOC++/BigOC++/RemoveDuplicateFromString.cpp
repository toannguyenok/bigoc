#include <vector>
#include <string>
using namespace std;

class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        int k = 1;
        vector<int> indexRemove;
        if (nums.size() == 0)
        {
            return 0;
        }
        if (nums.size() == 1)
        {
            return 1;
        }
        int lastValue = nums[0];

        for (int i = 1; i < nums.size(); i++)
        {
            if (nums[i] != lastValue)
            {
                k++;
            }
            else
            {
                indexRemove.push_back(i);
            }
            lastValue = nums[i];
        }
        for (int i = indexRemove.size() - 1; i >= 0; i--)
        {
            nums.erase(nums.begin() + indexRemove[i]);
        }
        return k;
    }
};