#include <iostream>
#include <cmath>

using namespace std;


int mainBeautiful() {
	int indexRow = -1;
	int indexColumn = -1;
	int temp;
	for (int i = 0; i < 5; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			cin >> temp;
			if (temp == 1)
			{
				indexRow = i;
				indexColumn = j;
			}
		}
	}
	int move = 0;
	move += abs(indexRow - 2);
	move += abs(indexColumn - 2);
	cout << move;

	return 0;
}