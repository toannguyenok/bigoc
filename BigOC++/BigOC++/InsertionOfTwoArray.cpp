#include <vector>
#include <string>
#include <algorithm>
#include <unordered_map>
using namespace std;

class Solution {
public:
    vector<int> intersect(vector<int>& nums1, vector<int>& nums2) {
        vector<int> result;
        unordered_map<int, int> umap;
        for (int i = 0; i < nums2.size(); i++)
        {
            if (umap.find(nums2[i]) == umap.end())
            {
                umap[nums2[i]] = 1;
            }
            else
            {
                int temp = umap[nums2[i]];
                temp++;
                umap[nums2[i]] = temp;
            }
        }
        for (int i = 0; i < nums1.size(); i++)
        {
            if (umap.find(nums1[i]) != umap.end())
            {
                int temp = umap[nums1[i]];
                if (temp > 0)
                {
                    result.push_back(nums1[i]);
                    temp--;
                    umap[nums1[i]] = temp;
                }
            }
        }
        return result;
    }
};